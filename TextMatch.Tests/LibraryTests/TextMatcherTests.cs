﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TextMatch.Library;

namespace TextMatch.Tests.LibraryTests
{
    [TestClass]
    public class TextMatcherTests
    {
        static readonly string InputText = "Polly put the kettle on, polly put the kettle on, polly put the kettle on we’ll all have tea";

        [TestMethod]
        public void ReturnsNullWhenNotSupliedWithEnoughVariables()
        {
            var matcher = new TextMatcher();
            var result = matcher.Match(null, null);
            Assert.AreEqual(null, result);
        }

        [TestMethod]
        public void FindOneLettersPositionInAString()
        {
            var matcher = new TextMatcher();
            var result = matcher.Match("132", "2");

            Assert.AreEqual("3", result);
        }

        [TestMethod]
        public void NoLettersPositionInAString()
        {
            var matcher = new TextMatcher();
            var result = matcher.Match("132", "4");

            Assert.AreEqual("There is no output", result);
        }

        [TestMethod]
        public void FindTwoLettersPositionInAString()
        {
            var matcher = new TextMatcher();
            var result = matcher.Match("1324", "32");

            Assert.AreEqual("2", result);
        }

        [TestMethod]
        public void FindMultipleTwoLettersPositionInAString()
        {
            var matcher = new TextMatcher();
            var result = matcher.Match("132432", "32");

            Assert.AreEqual("2,5", result);
        }

        [TestMethod]
        public void FindMultipleTwoLettersInRepeatingLetters()
        {
            var matcher = new TextMatcher();
            var result = matcher.Match("11111", "11");

            Assert.AreEqual("1,3", result);
        }

        [TestMethod]
        public void FindMultipleTwo_1LettersInRepeatingLetters()
        {
            var matcher = new TextMatcher();
            var result = matcher.Match("111111", "11");

            Assert.AreEqual("1,3,5", result);
        }

        [TestMethod]
        public void FindMultipleThreeLettersInRepeatingLetters()
        {
            var matcher = new TextMatcher();
            var result = matcher.Match("111111", "111");

            Assert.AreEqual("1,4", result);
        }

        [TestMethod]
        public void SubtextLongerThanTargetText()
        {
            var matcher = new TextMatcher();
            var result = matcher.Match("11", "11111");

            Assert.AreEqual("There is no output", result);
        }

        [TestMethod]
        public void TestCase1_2()
        {
            var matcher = new TextMatcher();
            var result = matcher.Match(InputText, "Polly");

            Assert.AreEqual("1,26,51", result);
        }

        [TestMethod]
        public void TestCase3()
        {
            var matcher = new TextMatcher();
            var result = matcher.Match(InputText, "ll");

            Assert.AreEqual("3,28,53,78,82", result);
        }

        [TestMethod]
        public void TestCase4()
        {
            var matcher = new TextMatcher();
            var result = matcher.Match(InputText, "X");

            Assert.AreEqual("There is no output", result);
        }

        [TestMethod]
        public void TestCase5()
        {
            var matcher = new TextMatcher();
            var result = matcher.Match(InputText, "Polx");

            Assert.AreEqual("There is no output", result);
        }
    }
}
