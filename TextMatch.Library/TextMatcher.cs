﻿using System.Collections.Generic;
using System.Text;
using System.Linq;
namespace TextMatch.Library
{
    /// <summary>
    /// Class defines Text Matcher object.
    /// </summary>
    public class TextMatcher : IMatchable
    {
        /// <summary>
        /// Matches <paramref name="subtext"/> against the <paramref name="text"/>
        /// </summary>
        /// <param name="text">Target</param>
        /// <param name="subtext">Query text</param>
        /// <returns>Outputs character positions in a string</returns>
        public string Match(string text, string subtext)
        {
            if (string.IsNullOrEmpty(text) || string.IsNullOrEmpty(subtext))
            {
                return null;
            }

            // As it is not clear in the description what should happen if there is given
            // situation like aaaa searching aa, I assume (should be cleared with PO/Stakeholder)
            // that aaaa, aa result is 1, 3 (RegEx global modifier)
            var cursor = 0;
            var positions = new List<string>();
            while(cursor < text.Length)
            {
                var sb = new StringBuilder();
                for (int j = 0; j < subtext.Length && cursor + j < text.Length; j++)
                {
                    sb.Append(text[cursor + j]);
                }

                // Let's be aware of different cultures
                if (sb.ToString().ToLowerInvariant() == subtext.ToLowerInvariant())
                {
                    // Position is counted as array position - 
                    // but result requires enumeration from 1 (position) 
                    positions.Add((cursor + 1).ToString());

                    // Moving cursor by a length of subtext if it was found
                    cursor += subtext.Length;
                }
                else
                {
                    cursor++;
                }
            }

            return positions.Count == 0
                ? "There is no output"
                : string.Join(",", positions);
        }
    }
}
