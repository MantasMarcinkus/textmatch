﻿using System.Collections.Generic;

namespace TextMatch.Library
{
    public interface IMatchable
    {
        string Match(string inputText, string subText);
    }
}