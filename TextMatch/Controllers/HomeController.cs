﻿using System.Web.Mvc;
using TextMatch.Library;

namespace TextMatch.Controllers
{
    public class HomeController : Controller
    {
        private IMatchable Matcher;

        public HomeController()
        {
            Matcher = new TextMatcher();
        }

        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Text match controller action
        /// </summary>
        /// <param name="text"></param>
        /// <param name="subtext"></param>
        /// <returns></returns>
        public ActionResult MatchText(string text, string subtext)
        {
            var result = Matcher.Match(text, subtext);

            return PartialView("_Result", result);
        }
    }
}