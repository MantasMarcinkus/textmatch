﻿using System.ComponentModel.DataAnnotations;

namespace TextMatch.Models
{
    public class TextMatcherModel
    {
        [Required]
        public string Text { get; set; }

        [Required]
        public string Subtext { get; set; }
    }
}